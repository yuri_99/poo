package projeto1;
import java.util.Scanner;

public class exerc3 {
  
	public static double lamp(double lar,double pro) {
		//18 = watts necessario para 1 m quadrado
		double necessario = (lar * pro)* 18;
		return necessario;	
	}
	
	public static void main(String[] args) {
		
	   double potencia,larg,pro;
	   Scanner entrada_lamp = new Scanner(System.in);
	   Scanner entrada_larg = new Scanner(System.in);
	   Scanner entrada_pro  = new Scanner(System.in);
	   
	   System.out.print("Digite a Potencia da lampada: ");
	   potencia = entrada_lamp.nextDouble();
	   System.out.print("Digite a largura do comodo: ");
	   larg = entrada_larg.nextDouble();
	   System.out.print("Digite a profundidade do comodo: ");
	   pro = entrada_pro.nextDouble();
	
	   System.out.print("Lampadas necessarias: "+ (lamp(larg,pro))/2);		
		
	}
	

}
